﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClientGui
{
    public partial class WelcomeClients : Form
    {
        public WelcomeClients()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ClientGui cw = new ClientGui();
            cw.ShowDialog();
        }

        private void button2_Click_1(object sender, EventArgs e)
        {

            DialogResult dr;

            dr = MessageBox.Show("Are you sure you want to quit?", "Are you sure???", MessageBoxButtons.YesNo, MessageBoxIcon.Hand, MessageBoxDefaultButton.Button2);

            if (dr == DialogResult.Yes)
                Close();
        }
    }
}
