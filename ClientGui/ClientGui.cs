﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;
using ClientLogic;
using CommonReasource;

//hbhbhb
namespace ClientGui
{
    public partial class ClientGui : Form
    {
        private Client client = new Client(RichTextBoxUpdate_static);

        private delegate void EnableDelegate(string str,string color);

        private static ClientGui thisForm = null;

        Color color = Color.Black;

        public ClientGui()
        {
            InitializeComponent();
            thisForm = this;

       
        }

        public static void RichTextBoxUpdate_static(string str,string color )
        {
            if (thisForm == null)
                return ;

            thisForm.RichTextBoxUpdate(str,color);



        }


        private void RichTextBoxUpdate(string str,string color)
        {
            // If this returns true, it means it was called from an external thread.
            if (InvokeRequired)
            {
                // Create a delegate of this method and let the form run it.
                this.Invoke(new EnableDelegate(RichTextBoxUpdate), new object[] { str,color });
                return; // Important
            }
            Color txtColor = Color.FromArgb(Convert.ToInt32(color));
        //    txtColor = Color.Red;
            this.richTextBox1.Select(richTextBox1.TextLength, 0);
            thisForm.richTextBox1.SelectionColor = txtColor;
            thisForm.richTextBox1.AppendText(str);

            
        }




        

        private void btnConnect_Click_1(object sender, EventArgs e)
        {
            int port;

            if (txtname.Text.Length == 0 || txtUsername.Text.Length == 0)
            {
                MessageBox.Show("Please choose a Name and Username");
                return;
            }


            try
            {
                IPAddress ip = IPAddress.Parse(txtServerIP.Text);

            }
            catch (Exception)
            {
                MessageBox.Show("Please enter a valid IP!"); 
                return;
            }

            try
            {
                port = int.Parse(txtServerPort.Text);

            }
            catch (Exception)
            {
                MessageBox.Show("Please enter valid port number");
                return;
            }
            try
            {
                client.Connect(txtServerIP.Text, port);
 
                if(radioButtonNewUser.Checked) // new user
                    client.SendUserData(ConnectionData.DATATYPE.registration,txtname.Text,txtUsername.Text);
                else // existing user
                {
                    client.SendUserData(ConnectionData.DATATYPE.connection, txtname.Text, txtUsername.Text);
                }

                Thread.Sleep(500); // todo: change it to a thread (unblocking code)

                if (!client.IsConnected())
                {
                    MessageBox.Show("Can't connect - please check your name and username\nOr maybe you are new?");
                    return;
                }

                UpdateControlsAferConnect();

            }
            catch (Exception ex)
            {
                MessageBox.Show("Can't connect to the server!\nPlease check the IP and the Port.");              
            }
            
        }


        private void UpdateControlsAferConnect()
        {
            btnConnect.Enabled = false;
            radioButtonExistingUser.Enabled = false;
            radioButtonNewUser.Enabled = false;
            btnDisconnect.Enabled = true;
            txtname.Enabled = false;
            txtUsername.Enabled = false;
            txtServerIP.Enabled = false;
            txtServerPort.Enabled = false;
            btnChangeColor.Enabled = false;
            btnSend.Enabled = true;
        }

        private void UpdateControlsAferDisconnect()
        {
            btnConnect.Enabled = true;
            radioButtonExistingUser.Enabled = true;
            radioButtonNewUser.Enabled = true;
            btnDisconnect.Enabled = false;
            txtname.Enabled = true;
            txtUsername.Enabled = true;
            txtServerIP.Enabled = true;
            txtServerPort.Enabled = true;
            btnChangeColor.Enabled = true;
            btnSend.Enabled = false;
        }

        private void btnSend_Click_1(object sender, EventArgs e)
        {
            SendMessage();
        }

        private void SendMessage()
        {
            if (txtMessage.Text.Length == 0)
            {
                MessageBox.Show("You can't send an empty message!!!");
                return;
            }
            try
            {
                //client.Send(txtMessage.Text);
                client.SendMessage(txtUsername.Text, txtMessage.Text, DateTime.Now, color.ToArgb().ToString()); // todo: add color picker
                txtMessage.Text = "";

            }
            catch (Exception)
            {

                throw;
            }
            
        }

        private void btnDisconnect_Click(object sender, EventArgs e)
        {
            try
            {
                client.Disconnect();
                UpdateControlsAferDisconnect();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                throw;
            }
        }



        private void txtMessage_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                SendMessage();

        }

        private void btnChangeColor_Click(object sender, EventArgs e)
        {
            ColorDialog MyDialog = new ColorDialog();
            // Keeps the user from selecting a custom color.
            MyDialog.AllowFullOpen = true;
            // Allows the user to get help. (The default is false.)
            MyDialog.ShowHelp = true;
            // Sets the initial color select to the current text color.
            //MyDialog.Color = textBox1.ForeColor;
 
            // Update the text box color if the user clicks OK 
            if (MyDialog.ShowDialog() == DialogResult.OK)
            {
                color = MyDialog.Color;
            }
        }
    }
}
