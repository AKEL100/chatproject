﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;


namespace CommonReasource
{
    [Serializable]
    public class MessageData
    {
        private string _name;
        private string _message;
        private DateTime _timeStamp;
        private string _color;
        private ConsoleColor color;

        public MessageData(string nickName,string message, DateTime timeStamp, string color)
        {
            _message = message;
            _timeStamp = timeStamp;
            _color = color;
            _name = nickName;
        }

        public MessageData()
        {
            _message = "Empty constructor";
            _timeStamp = DateTime.Now;
            _color = "red";
            _name = "Empty constructor";
            
        }

        public string Color
        {
            get { return _color; }
        }

        public string Name
        {
            get { return _name; }
        }

        public DateTime TimeStamp
        {
            get { return _timeStamp; }
        }

        public string Message
        {
            get { return _message; }
        }
    }
}
