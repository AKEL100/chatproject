﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonReasource
{

    [Serializable]
    public class ConnectionData
    {
        public enum DATATYPE : int
        {
            connection,
            registration
        }

        private DATATYPE dataType; // connection or registration

        private string _name;
        private string _userName;

        public ConnectionData(DATATYPE dataType, string name, string userName)
        {
            this.dataType = dataType;
            _name = name;
            _userName = userName;
        }

        public DATATYPE DataType
        {
            get { return dataType; }
        }

        public string Name
        {
            get { return _name; }
        }

        public string UserName
        {
            get { return _userName; }
        }
    }



}
