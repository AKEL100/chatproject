﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace CommonReasource
{
    public class ConnectedClient
    {
        private string _name;
        private Socket socket;

        public ConnectedClient(string name, Socket socket)
        {
            _name = name;
            this.socket = socket;
        }

        public string Name
        {
            get { return _name; }
        }

        public Socket Socket
        {
            get { return socket; }
        }
    }
}
