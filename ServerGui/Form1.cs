﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ServerLogic;
using CommonReasource;

namespace ServerGui
{
    public partial class Form1 : Form
    {
        Server server = new Server(ListBoxUpdate_Static);

        private delegate void EnableDelegate(ConnectedClient clientsList,Boolean isConnected );

        static private Form1 thisForm = null;

        public Form1()
        {
            InitializeComponent();
            thisForm = this;
            
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            try
            {
                IPAddress.Parse(txtIP.Text);
            }
            catch (Exception)
            {

                MessageBox.Show("Please enter valid IP");
                return;
            }
            try
            {
                int.Parse(txtPort.Text);
            }
            catch (Exception)
            {

                MessageBox.Show("Please enter valid Port");
                return;
            }
            try
            {
                server.StartListen(txtIP.Text,int.Parse(txtPort.Text));
                btnStart.Enabled = false;
                btnStop.Enabled = true;
                txtIP.Enabled = false;
                txtPort.Enabled = false;
                server.SqlServer.ClearUsersConnected();
    
            }
            catch (Exception)
            {

                MessageBox.Show("Can't start the server, please choose another port or ip");
            }

        }

        private void WindowClosed(object sender, FormClosingEventArgs e)
        {
            try
            {
                server.Stop();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());

            }

        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            try
            {
                server.Stop();
                btnStart.Enabled = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());

            }
            btnStop.Enabled = false;
            txtIP.Enabled = true;
            txtPort.Enabled = true;
        }


        public static void ListBoxUpdate_Static(ConnectedClient client, bool isConnected)
        {
            if (thisForm == null)
                return;

            thisForm.ListBoxUpdate(client, isConnected);

        }


        public void ListBoxUpdate(ConnectedClient client,bool isConnected)
        {
            // If this returns true, it means it was called from an external thread.
            if (InvokeRequired)
            {
                // Create a delegate of this method and let the form run it.
                this.Invoke(new EnableDelegate(ListBoxUpdate), new object[] { client,isConnected });
                return; // Important
            }

            if (isConnected)
            {
                listUsers.Items.Add(client.Name);
                richTextBoxHisory.AppendText(DateTime.Now.ToShortDateString()+":"+DateTime.Now.ToShortTimeString() + " [" + client.Name + "] Connected\n");
                richTextBoxHisory.SelectionStart = richTextBoxHisory.Text.Length;
                richTextBoxHisory.ScrollToCaret();
                
            }
                
            else
            {
                listUsers.Items.Remove(client.Name);
                richTextBoxHisory.AppendText(DateTime.Now.ToShortDateString()+":"+DateTime.Now.ToShortTimeString() + " [" + client.Name + "] Disconnected\n");
                richTextBoxHisory.SelectionStart = richTextBoxHisory.Text.Length;
                richTextBoxHisory.ScrollToCaret();
            }

        }

        private void btnViewMessages_Click(object sender, EventArgs e)
        {
            MessagesView messagesView = new MessagesView(server.SqlServer);
            messagesView.ShowDialog();
        }

        private void btn_RemoveUsers_Click(object sender, EventArgs e)
        {
            RemoveUsersFromDB removeUsers = new RemoveUsersFromDB(server.SqlServer);
            removeUsers.ShowDialog();
        }

        }

}
