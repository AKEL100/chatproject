﻿namespace ServerGui
{
    partial class MessagesView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dataGridViewMessages = new System.Windows.Forms.DataGridView();
            this.chatDBDataSet = new ServerGui.ChatDBDataSet();
            this.messageBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.messageTableAdapter = new ServerGui.ChatDBDataSetTableAdapters.MessageTableAdapter();
            this.txtUsernameFilter = new System.Windows.Forms.TextBox();
            this.btnSearch = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtSearchKeyword = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.dateTimePickerSendDate = new System.Windows.Forms.DateTimePicker();
            this.checkBoxFilterByDate = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewMessages)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chatDBDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.messageBindingSource)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridViewMessages
            // 
            this.dataGridViewMessages.AllowUserToAddRows = false;
            this.dataGridViewMessages.AllowUserToDeleteRows = false;
            this.dataGridViewMessages.AllowUserToOrderColumns = true;
            this.dataGridViewMessages.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewMessages.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewMessages.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewMessages.Location = new System.Drawing.Point(12, 162);
            this.dataGridViewMessages.Name = "dataGridViewMessages";
            this.dataGridViewMessages.ReadOnly = true;
            this.dataGridViewMessages.RowTemplate.Height = 24;
            this.dataGridViewMessages.Size = new System.Drawing.Size(977, 345);
            this.dataGridViewMessages.TabIndex = 0;
            // 
            // chatDBDataSet
            // 
            this.chatDBDataSet.DataSetName = "ChatDBDataSet";
            this.chatDBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // messageBindingSource
            // 
            this.messageBindingSource.DataMember = "Message";
            this.messageBindingSource.DataSource = this.chatDBDataSet;
            // 
            // messageTableAdapter
            // 
            this.messageTableAdapter.ClearBeforeFill = true;
            // 
            // txtUsernameFilter
            // 
            this.txtUsernameFilter.Location = new System.Drawing.Point(99, 27);
            this.txtUsernameFilter.Name = "txtUsernameFilter";
            this.txtUsernameFilter.Size = new System.Drawing.Size(165, 22);
            this.txtUsernameFilter.TabIndex = 1;
            this.txtUsernameFilter.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtUsernameFilter_KeyPress);
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(870, 26);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(75, 23);
            this.btnSearch.TabIndex = 2;
            this.btnSearch.Text = "Search";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 17);
            this.label1.TabIndex = 3;
            this.label1.Text = "Username:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.checkBoxFilterByDate);
            this.groupBox1.Controls.Add(this.dateTimePickerSendDate);
            this.groupBox1.Controls.Add(this.txtSearchKeyword);
            this.groupBox1.Controls.Add(this.txtUsernameFilter);
            this.groupBox1.Controls.Add(this.btnSearch);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 30);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(977, 96);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Search filter";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(292, 30);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(66, 17);
            this.label2.TabIndex = 3;
            this.label2.Text = "Keyword:";
            // 
            // txtSearchKeyword
            // 
            this.txtSearchKeyword.Location = new System.Drawing.Point(375, 27);
            this.txtSearchKeyword.Name = "txtSearchKeyword";
            this.txtSearchKeyword.Size = new System.Drawing.Size(165, 22);
            this.txtSearchKeyword.TabIndex = 1;
            this.txtSearchKeyword.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSearchKeyword_KeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(556, 30);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(42, 17);
            this.label3.TabIndex = 3;
            this.label3.Text = "Date:";
            // 
            // dateTimePickerSendDate
            // 
            this.dateTimePickerSendDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePickerSendDate.Location = new System.Drawing.Point(648, 25);
            this.dateTimePickerSendDate.Name = "dateTimePickerSendDate";
            this.dateTimePickerSendDate.Size = new System.Drawing.Size(200, 22);
            this.dateTimePickerSendDate.TabIndex = 4;
            // 
            // checkBoxFilterByDate
            // 
            this.checkBoxFilterByDate.AutoSize = true;
            this.checkBoxFilterByDate.Location = new System.Drawing.Point(648, 53);
            this.checkBoxFilterByDate.Name = "checkBoxFilterByDate";
            this.checkBoxFilterByDate.Size = new System.Drawing.Size(120, 21);
            this.checkBoxFilterByDate.TabIndex = 5;
            this.checkBoxFilterByDate.Text = "Filter by date?";
            this.checkBoxFilterByDate.UseVisualStyleBackColor = true;
            // 
            // MessagesView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1001, 588);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.dataGridViewMessages);
            this.Name = "MessagesView";
            this.Text = "MessagesView";
            this.Load += new System.EventHandler(this.MessagesView_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewMessages)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chatDBDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.messageBindingSource)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridViewMessages;
        private ChatDBDataSet chatDBDataSet;
        private System.Windows.Forms.BindingSource messageBindingSource;
        private ChatDBDataSetTableAdapters.MessageTableAdapter messageTableAdapter;
        private System.Windows.Forms.TextBox txtUsernameFilter;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtSearchKeyword;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dateTimePickerSendDate;
        private System.Windows.Forms.CheckBox checkBoxFilterByDate;
    }
}