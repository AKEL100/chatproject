﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ServerLogic;

namespace ServerGui
{
    public partial class RemoveUsersFromDB : Form
    {
        private SqlServer sqlServer;

        DataTable dt = null;


        public RemoveUsersFromDB(SqlServer sqlServer)
        {
            InitializeComponent();

            this.sqlServer = sqlServer;

            dt = sqlServer.GetUsers();

            dataGridView1.DataSource = dt;

        }

        private void btn_Delete_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count == 0)
            {
                MessageBox.Show("Please select user before delete");
                return;
            }
            int selectedUserId = (int)dataGridView1.SelectedRows[0].Cells[0].Value;
            sqlServer.DeleteUsereByID(selectedUserId);
            dataGridView1.Rows.RemoveAt(dataGridView1.SelectedRows[0].Index);
        }
    }
}
