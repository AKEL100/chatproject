﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ServerLogic;

namespace ServerGui
{
    public partial class MessagesView : Form
    {
        private SqlServer sqlServer;

        DataTable dt = null;

        public MessagesView(SqlServer sqlServer)
        {
            InitializeComponent();
            this.sqlServer = sqlServer;

            dt = sqlServer.GetMessages();
        }

        private void MessagesView_Load(object sender, EventArgs e)
        {
            dataGridViewMessages.DataSource = sqlServer.GetMessages();
        }



        private void btnSearch_Click(object sender, EventArgs e)
        {
            SearchFilter();

        }

        private void txtUsernameFilter_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char) 13)
            {
                SearchFilter();

            }
        }

        private void SearchFilter()
        {
            DataView dv = dt.DefaultView;
            if (checkBoxFilterByDate.Checked)
            {
                string dateFilterString = dateTimePickerSendDate.Value.ToString("dd/MM/yyyy");
                DateTime dateFilter = dateTimePickerSendDate.Value;
                string filterString = string.Format("username like '%{0}%' AND MessageText like '%{1}%' AND Convert(sendDate,'System.String') Like  '{2}*'", txtUsernameFilter.Text, txtSearchKeyword.Text, dateFilterString);
                dv.RowFilter = filterString;
            }
                
            else
            {
                dv.RowFilter = string.Format("username like '%{0}%' AND MessageText like '%{1}%' ", txtUsernameFilter.Text, txtSearchKeyword.Text);
                
            }
                
            
            dataGridViewMessages.DataSource = dv.ToTable();
            dataGridViewMessages.Refresh();   
        }

        private void txtSearchKeyword_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                SearchFilter();

            }

        }

 
    }
}
