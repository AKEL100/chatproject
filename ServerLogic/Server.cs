﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using CommonReasource;

namespace ServerLogic
{
    public class Server
    {
        static TcpListener myListener;
        IFormatter formatter;
        static private bool ServerRunningFlag = true;
        private List<ConnectedClient> connectedClients;
        Action<ConnectedClient,bool> _newClientConnectedHandle; // The function that will be called when a new client is connect
        private SqlServer sqlServer;

        public SqlServer SqlServer
        {
            get { return sqlServer; }
        }

        public Server(Action<ConnectedClient,bool> newClientConnectedHandle)
        {

            connectedClients = new List<ConnectedClient>();
            formatter = new BinaryFormatter();
            _newClientConnectedHandle = newClientConnectedHandle;
            sqlServer = new SqlServer();
        }
        

        public void StartListen(string ipAddress, int port)
        {
            try
            {
                IPAddress ipAd = IPAddress.Parse(ipAddress);
                // Initializes the Listener
                myListener = new TcpListener(ipAd, port);
                // Start Listeneting at the specified port
                myListener.Start();
                ServerRunningFlag = true;
                Task.Factory.StartNew(() => StartServerHandler()); // Start handle new connections in a new thread
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public void Stop()
        {
            ServerRunningFlag = false;
        }


        private void StartServerHandler()
        {
            while (ServerRunningFlag)
            {
                while (!myListener.Pending() && ServerRunningFlag) // wait until new client is try to connect or stop request
                {
                    Thread.Sleep(100);
                }

                if (!ServerRunningFlag)
                    break;

                Socket s = myListener.AcceptSocket();


                UserToken currentUserToken = AuthenticateClient(s);

                if (!currentUserToken.Authorized) // Unsuccessfull authentication
                {
                    s.Close();
                    continue;

                }

                Task.Factory.StartNew(() => HandleClient(s, currentUserToken.UserName)); // new thread for each new client
            }

            myListener.Stop(); 
        }


        void HandleClient(Socket socket,string userName)
        {
            MessageData packet;

            bool clientConnected = true;

          

            Stream str;

            while (clientConnected)
            {
                try
                {
                    str = new NetworkStream(socket);
                    packet = (MessageData)formatter.Deserialize(str);
                    if (userName == "empty")
                        userName = packet.Name;
                }
                catch (Exception)
                {
                    clientConnected = false;
                    continue;
                }

                //if (packet.Message == "#Connected!")
                //{
                //    ConnectedClient newClient = new ConnectedClient(packet.Name, socket);
                //    connectedClients.Add(newClient);// Add the specific socket (user ip+port) to the list
                //    _newClientConnectedHandle(newClient,true);
                //    continue;
                //}
                sqlServer.AddMessage(packet.Message,packet.Name);
                BroadcastMessage(packet);
            }


            ConnectedClient removedClient = connectedClients.Find(x => x.Socket == socket); //todo: change to gui display from sql
            connectedClients.Remove(removedClient); // remove the client from the list of clients
            sqlServer.UpdateUserDisconnected(userName);

            _newClientConnectedHandle(removedClient,false); // remove the client from the list of connected clients on server
            socket.Close(); // close the connection

        }



        private void AddNewConnectedClient(string userName,Socket socket )
        {
            ConnectedClient newClient = new ConnectedClient(userName, socket);
            connectedClients.Add(newClient);// Add the specific socket (user ip+port) to the list
            _newClientConnectedHandle(newClient, true);
            sqlServer.UpdateUserConnected(userName);
        }


        UserToken AuthenticateClient(Socket socket)
        {
            ConnectionData connectionData;

            bool clientConnected = true;

            while (clientConnected)
            {
                try
                {
                    Stream str = new NetworkStream(socket);
                    connectionData = (ConnectionData)formatter.Deserialize(str);
                }
                catch (Exception)
                {
                    clientConnected = false;
                    continue;
                }

                // what to do for existing user
                if (connectionData.DataType == ConnectionData.DATATYPE.connection)
                {
                    // Check if the user and username are right
                    if (!sqlServer.IsUserExist(connectionData.Name, connectionData.UserName))
                        return new UserToken(connectionData.UserName,false);

                    // Check if the user not allready connected
                    if (sqlServer.IsUserConnected(connectionData.UserName))
                        return new UserToken(connectionData.UserName, false);

                    AddNewConnectedClient(connectionData.UserName, socket);



                    return new UserToken(connectionData.UserName, true); ;
                 //   ConnectedClient newClient = new ConnectedClient(packet.Name, socket);
                 //   connectedClients.Add(newClient);// Add the specific socket (user ip+port) to the list
                 //   _newClientConnectedHandle(newClient, true);

                }
                else // what to do for new client
                {
                    if (sqlServer.IsUserNameExist(connectionData.UserName)) // if the username is exist - return false bacause it's used
                        return new UserToken(connectionData.UserName, false);

                    sqlServer.AddUser(connectionData.Name,connectionData.UserName);
                    
                    AddNewConnectedClient(connectionData.UserName, socket);

                    return new UserToken(connectionData.UserName, true);
                }

            }

            return new UserToken("unknown", false); ; // unknown user
        }

        private void BroadcastMessage(MessageData packet) // sends the message to all clients
        {
            ASCIIEncoding asen = new ASCIIEncoding();

            foreach (var clientSocket in connectedClients) // all clients in the list
            {
                try
                {

                    Stream stream = new NetworkStream(clientSocket.Socket);// send the message to the specific client
                    formatter.Serialize(stream,packet);
                }
                catch (Exception)
                {

                    
                }

            }
        }




    }
}
