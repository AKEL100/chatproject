﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServerLogic
{
    public class UserToken
    {
        private string userName;
        private bool authorized;

        public UserToken(string userName, bool authorized)
        {
            this.userName = userName;
            this.authorized = authorized;
        }

        public string UserName
        {
            get { return userName; }
            set { userName = value; }
        }

        public bool Authorized
        {
            get { return authorized; }
            set { authorized = value; }
        }
    }
}
