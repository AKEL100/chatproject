﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using CommonReasource;

namespace ServerLogic
{
    public class SqlServer
    {
        private SqlConnection sqlCon;


        //create connetion to DB
        private static SqlConnection Connect()
        {
            string cStr = @"Data Source=.\SQLEXPRESS;Initial Catalog=ChatDB;Integrated Security=True";
            SqlConnection con = new SqlConnection(cStr);
            con.Open();
            return con;
        }

        //create sql command
        private static SqlCommand CreateCommand(string CommandSTR, SqlConnection con)
        {
            SqlCommand cmd = new SqlCommand(CommandSTR, con);
            cmd.CommandTimeout = 10;
            cmd.CommandType = System.Data.CommandType.Text;

            return cmd;

        }

        //exec sql command
        private static void ExecuteAndClose(SqlCommand cmd)
        {
            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw (ex);
            }

            finally
            {
                cmd.Connection.Close();
            }
        }


        //get rows from DB
        private static DataTable Select(SqlCommand cmd)
        {
            try
            {
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds, "Table");
                DataTable dt = ds.Tables["Table"];

                return dt;
            }
            catch (Exception ex)
            {
                throw (ex);
            }

            finally
            {
                cmd.Connection.Close();
            }

        }

        public DataTable GetMessages()
        {
            SqlConnection sqlConnection = Connect();

            SqlCommand sqlCom = new SqlCommand("select U.username,M.MessageText,M.sendDate from [dbo].[User] as U,[dbo].[Message] as M where U.ID = M.UserID", sqlConnection); // sqlCommand
           // sqlCom.Parameters.AddWithValue("@username", userName);
            return Select(sqlCom);

        }

        public DataTable GetUsers()
        {
            SqlConnection sqlConnection = Connect();

            SqlCommand sqlCom = new SqlCommand("select * from [dbo].[User]", sqlConnection); // sqlCommand
            // sqlCom.Parameters.AddWithValue("@username", userName);
            return Select(sqlCom);

        }

        public void DeleteUsereByID(int userId)
        {
            SqlConnection sqlConnection = Connect();
            SqlCommand sqlCom1 = new SqlCommand("delete from [dbo].[Message] where UserID=@userID", sqlConnection); // sqlCommand
            SqlCommand sqlCom2 = new SqlCommand("delete from [dbo].[User] where ID=@userID", sqlConnection); // sqlCommand
            
            sqlCom1.Parameters.AddWithValue("@userID", userId);
            sqlCom1.ExecuteNonQuery();
            sqlCom2.Parameters.AddWithValue("@userID", userId);
            sqlCom2.ExecuteNonQuery();
        }

        //convert dataTable to list
        private static List<T> ConvertDataTable<T>(DataTable dt)
        {
            List<T> data = new List<T>();
            foreach (DataRow row in dt.Rows)
            {
                T item = GetItem<T>(row);
                data.Add(item);
            }
            return data;
        }

        //get the row item according to the list type
        private static T GetItem<T>(DataRow dr)
        {
            Type temp = typeof(T);
            T obj = Activator.CreateInstance<T>();

            foreach (DataColumn column in dr.Table.Columns)
            {
                foreach (PropertyInfo pro in temp.GetProperties())
                {
                    if (pro.Name == column.ColumnName)
                        pro.SetValue(obj, dr[column.ColumnName], null);
                    else
                        continue;
                }
            }
            return obj;
        }


        public void AddUser(string name, string username)
        {
            SqlConnection sqlConnection = Connect();

            SqlCommand sqlCom = new SqlCommand("insert into [dbo].[User] values(@name,@username,@lastConnectionDate,@isConnected)", sqlConnection); // sqlCommand
            sqlCom.Parameters.AddWithValue("@name", name);
            sqlCom.Parameters.AddWithValue("@username", username);
            sqlCom.Parameters.AddWithValue("@lastConnectionDate", DateTime.Now);
            sqlCom.Parameters.AddWithValue("@isConnected", true);

            sqlCom.ExecuteNonQuery();
        }

        public void AddMessage(string messageText,string userName)
        {
            SqlConnection sqlConnection = Connect();

            int userId = GetIdByUserName(userName);

            SqlCommand sqlCom = new SqlCommand("insert into [dbo].[Message] values(@messageText,@userID,@sendDate)", sqlConnection); // sqlCommand
            sqlCom.Parameters.AddWithValue("@messageText", messageText);
            sqlCom.Parameters.AddWithValue("@userID", userId);
            sqlCom.Parameters.AddWithValue("@sendDate", DateTime.Now);
            

            sqlCom.ExecuteNonQuery();
        }

        private int GetIdByUserName(string userName)
        {
            SqlConnection sqlConnection = Connect();

            SqlCommand sqlCom = new SqlCommand("SELECT ID FROM [dbo].[User] Where username=@username", sqlConnection); // sqlCommand
            sqlCom.Parameters.AddWithValue("@username", userName);
           
            using (SqlDataReader reader = sqlCom.ExecuteReader())
            {
                reader.Read(); // read first row
                int userId = reader.GetInt32(0);

                return userId;

            }
        }

        public bool IsUserExist(string name,string username) // Check if there are user and username combination
        {
            SqlConnection sqlConnection = Connect();

            SqlCommand sqlCom = new SqlCommand("SELECT * FROM [dbo].[User] Where name=@name AND username=@username", sqlConnection); // sqlCommand
            sqlCom.Parameters.AddWithValue("@username", username);
            sqlCom.Parameters.AddWithValue("@name", name);

             using (SqlDataReader reader = sqlCom.ExecuteReader())
             {
                 if (reader.HasRows)
                     return true;

             }
            return false;
        }

        public bool IsUserNameExist(string username) // Check if the current username is used (exist)
        {
            SqlConnection sqlConnection = Connect();

            SqlCommand sqlCom = new SqlCommand("SELECT * FROM [dbo].[User] Where username=@username", sqlConnection); // sqlCommand
            sqlCom.Parameters.AddWithValue("@username", username);

            using (SqlDataReader reader = sqlCom.ExecuteReader())
            {
                if (reader.HasRows)
                    return true;

            }
            return false;
        }

        public bool IsUserConnected(string username) // Check if the current username is used (exist)
        {
            SqlConnection sqlConnection = Connect();

            SqlCommand sqlCom = new SqlCommand("SELECT * FROM [dbo].[User] Where username=@username and isConnected='true'", sqlConnection); // sqlCommand
            sqlCom.Parameters.AddWithValue("@username", username);

            using (SqlDataReader reader = sqlCom.ExecuteReader())
            {
                if (reader.HasRows)
                    return true;

            }
            return false;
        }

        public bool UpdateUserDisconnected(string username)
        {
            SqlConnection sqlConnection = Connect();

            SqlCommand sqlCom = new SqlCommand("UPDATE [dbo].[User] set IsConnected = 'false' where username=@username", sqlConnection); // sqlCommand
            sqlCom.Parameters.AddWithValue("@username", username);
            sqlCom.ExecuteNonQuery();
            return true;
        }

        public bool ClearUsersConnected() // set all users to "disconnected" 
        {
            SqlConnection sqlConnection = Connect();

            SqlCommand sqlCom = new SqlCommand("UPDATE [dbo].[User] set IsConnected = 'false'", sqlConnection); // sqlCommand
            sqlCom.ExecuteNonQuery();
            return true;
        }

        public bool UpdateUserConnected(string username)
        {
            SqlConnection sqlConnection = Connect();

            SqlCommand sqlCom = new SqlCommand("UPDATE [dbo].[User] set IsConnected = 'true',LastConnectionDate=@lastConnectionDate where username=@username", sqlConnection); // sqlCommand
            sqlCom.Parameters.AddWithValue("@username", username);
            sqlCom.Parameters.AddWithValue("@lastConnectionDate", DateTime.Now);
            sqlCom.ExecuteNonQuery();
            return true;
        }
    }
}
