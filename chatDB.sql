Use Master
GO

CREATE DATABASE ChatDB
GO

Use ChatDB 
GO


CREATE TABLE "User" (
    [ID] int IDENTITY (1, 1) NOT NULL ,
	[Name] nvarchar(50) NOT NULL,
	[UserName] nvarchar(50) NOT NULL UNIQUE,
	[LastConnectionDate] datetime not null,
	[IsConnected] bit
	Primary Key([ID]),
) 
GO 

CREATE TABLE [Message] (
    [ID] int IDENTITY (1, 1) NOT NULL ,
	[MessageText] nvarchar(MAX) NOT NULL,
	[UserID] int,
	[SendDate] datetime not null,
	
	Primary Key(ID),
	FOREIGN KEY ([UserID]) REFERENCES [dbo].[User](ID)
) 
GO 


-- insert into [dbo].[User] values('maor','tanami','12/3/2017','true')

--insert into [dbo].[Message] values('text',1,'12/3/2017')

--select * from [dbo].[User]
--select * from [dbo].[Message]

--select * from [dbo].[User] as U,[dbo].[Message] as M where U.ID = M.UserID