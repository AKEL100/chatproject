﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using CommonReasource;


namespace ClientLogic
{
    public class Client
    {
        private TcpClient tcpclnt;
        private Stream tcpStream; // required for transter a data between the client and the server
        private bool connectedFlag = false; // to determine if the client is still connected
        Action<string,string> newMessageHandler; // The function that will be called when a new message received from server
   
   
        IFormatter formatter = new BinaryFormatter();
        

        public Client(Action<string,string> newMessagehandlerFunction)
        {
            newMessageHandler = newMessagehandlerFunction;
            formatter = new BinaryFormatter();
        }

 

        public void Connect(string ipAdress,int port )
        {
            try
            {
                tcpclnt = new TcpClient();
                tcpclnt.Connect(ipAdress, port); // Use the ipaddress as in the server program               
                tcpStream = tcpclnt.GetStream(); // get the stream 
                connectedFlag = true; 
       
                Task.Factory.StartNew(() => WaitForMessages());

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Disconnect()
        {
            try
            {
                connectedFlag = false;
                tcpclnt.Close();
                
            }
            catch (Exception)
            {

            }
            
        }

        public bool IsConnected()
        {
            return connectedFlag;
        }

        public void Send(string message)
        {
            if (!tcpclnt.Connected)
            {
                return;
            }

            if (message.Length == 0)
            {
                return;
            }

            ASCIIEncoding asen = new ASCIIEncoding();
            byte[] ba = asen.GetBytes(message);

            tcpStream.Write(ba, 0, ba.Length); // send the data
  
        }

        public void SendMessage(string userName,string message,DateTime dateTime,string color)
        {

            MessageData packet = new MessageData(userName, message, DateTime.Now, color);

            if (!tcpclnt.Connected)
            {
                return;
            }

            if (message.Length == 0)
            {
                return;

            }

           // ASCIIEncoding asen = new ASCIIEncoding();
         //   byte[] ba = asen.GetBytes(message);

          //  tcpStream.Write(ba, 0, ba.Length); // send the data

           // _xmlSerializer.Serialize(tcpStream, packet);
            formatter.Serialize(tcpStream,packet);

            //  MessageBox.Show("Replied: " + reply);

            //   tcpclnt.Close();

        }

        public void SendUserData(ConnectionData.DATATYPE datatype,string name,string userName)
        {

        //    MessageData packet = new MessageData(nickName, message, DateTime.Now, color);

            ConnectionData connectionData = new ConnectionData(datatype,name,userName);

            if (!tcpclnt.Connected)
            {
                return;
            }

          //  ASCIIEncoding asen = new ASCIIEncoding();
        //    byte[] ba = asen.GetBytes(message);

            //  tcpStream.Write(ba, 0, ba.Length); // send the data

            // _xmlSerializer.Serialize(tcpStream, packet);
            formatter.Serialize(tcpStream, connectionData);

            //  MessageBox.Show("Replied: " + reply);

            //   tcpclnt.Close();

        }

        private void WaitForMessages()
        {
           // byte[] bb = new byte[100];
          //  int dataSize;
            MessageData packet;

              while (connectedFlag)
            {
                try
                {
                  //  dataSize = tcpStream.Read(bb, 0, bb.Length);
                 //   Stream str = new NetworkStream(socket);
                    packet = (MessageData)formatter.Deserialize(tcpStream);
                }
                catch (Exception)
                {
                    connectedFlag = false;
                    continue;
                }

                //if(dataSize == 0)
                //    continue;

                string reply = "Message From " + packet.Name + ": " + packet.Message ;

                //for (int i = 0; i < dataSize; i++)
                //{
                //    reply += Convert.ToChar(bb[i]);
                //}

                newMessageHandler(reply + "\n",packet.Color); // run the external function (update the richTextbox)
            }

            Console.WriteLine("sdsd");
        }




















    }
}
